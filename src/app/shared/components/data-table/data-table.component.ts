import {
    Component,
    OnInit,
    ViewChild,
    Input,
    Output,
    EventEmitter,
    AfterViewChecked,
    Renderer2,
    OnChanges,
    SimpleChanges
} from '@angular/core';
import { DataTableOption } from '@app/core/models/options/data-table.option';
import { DataTableSearchModel } from '@app/core/models/data/data-table-search.model';

import 'datatables.net';
import * as _ from 'lodash';
import { PagingRequest } from '@app/core/models/api-data/base.request';
import { AlertService } from '@app/core/services/common/alert.service';

@Component({
    selector: 'common-data-table',
    templateUrl: './data-table.component.html',
    styleUrls: ['./data-table.component.scss']
})
export class DataTableComponent implements OnInit, AfterViewChecked, OnChanges {
    //#region Inputs, Outputs

    @Input() request?: PagingRequest;
    @Input() options: DataTableOption;
    @Input() title: string;

    @Output() addClick: EventEmitter<any> = new EventEmitter();
    @Output() editClick: EventEmitter<any> = new EventEmitter();
    @Output() deleteClick: EventEmitter<any> = new EventEmitter();
    @Output() viewClick: EventEmitter<any> = new EventEmitter();

    //#endregion

    //#region Properties

    @ViewChild('dataTable', { static: true }) tableElement;
    table: JQuery;

    private hasAttachedListenerEdit = false;
    private hasAttachedListenerDelete = false;
    private hasAttachedListenerView = false;

    hasAdd = false;
    hasEdit = false;
    hasDelete = false;
    hasView = false;

    //#endregion

    //#region Constructors

    constructor(private renderer: Renderer2, private alertService: AlertService) {
    }

    //#endregion

    //#region OnInit

    ngOnInit() {

    }

    ngOnChanges(changes: SimpleChanges) {
        this.init();
    }

    ngAfterViewChecked() {
        if (!this.hasAttachedListenerEdit) {
            const buttons = this.tableElement.nativeElement.querySelectorAll('#editButton');
            if (buttons.length > 0) {
                // tslint:disable-next-line: prefer-for-of
                for (let i = 0; i < buttons.length; i++) {
                    this.renderer.listen(buttons[i], 'click', (evt) => {
                        this.editClick.emit(evt);
                    });
                }
                this.hasAttachedListenerEdit = true;
            }
        }

        if (!this.hasAttachedListenerDelete) {
            const buttons = this.tableElement.nativeElement.querySelectorAll('#deleteButton');
            if (buttons.length > 0) {
                // tslint:disable-next-line: prefer-for-of
                for (let i = 0; i < buttons.length; i++) {
                    this.renderer.listen(buttons[i], 'click', (evt) => {
                        this.deleteClick.emit(evt);
                    });
                }
                this.hasAttachedListenerDelete = true;
            }
        }

        if (!this.hasAttachedListenerView) {
            const rows = this.tableElement.nativeElement.querySelectorAll('.table-row');
            if (rows.length > 0) {
                // tslint:disable-next-line: prefer-for-of
                for (let i = 0; i < rows.length; i++) {
                    this.renderer.listen(rows[i], 'click', (evt) => {
                        this.viewClick.emit(evt);
                    });
                }
                this.hasAttachedListenerView = true;
            }
        }
    }

    //#endregion

    //#region Functions

    init() {
        $.fn.dataTable.ext.errMode = 'none';

        if (!this.options) {
            this.options = new DataTableOption({
                drawCallback: () => {
                    this.hasAttachedListenerEdit = false;
                    this.hasAttachedListenerDelete = false;
                    this.hasAttachedListenerView = false;
                },
            });
        } else {
            this.hasAdd = this.options.actions.includes('Add');
            this.hasEdit = this.options.actions.includes('Edit');
            this.hasDelete = this.options.actions.includes('Delete');
            this.hasView = this.options.actions.includes('View');

            if (this.hasEdit || this.hasDelete) {
                this.options.columns.push({
                    title: '',
                    data: null,
                    orderable: false,
                    width: (this.hasEdit && this.hasDelete) ? '200px' : '100px',
                    className: 'table-col-action',
                    defaultContent: (this.hasEdit ? '<button id=editButton type="button" class="btn btn-outline-primary"><i class="fa fa-edit"></i> Edit</button>' : '')
                        + (this.hasDelete ? '<button id=deleteButton type="button" class="btn btn-outline-danger"><i class="fa fa-trash"></i> Delete</button>' : '')
                });
            }

            if (this.hasView) {
                this.options.createdRow = (row, data) => {
                    $(row).find('td').addClass('table-row');
                    if (_.find(this.options.columns, { title: 'Actions' })) {
                        $(row).find('td:last-child').removeClass('table-row');
                    }
                };
            }

            this.options.drawCallback = () => {
                this.rebindEvent();
            };

            if (this.options.serverSide) {
                this.options.deferLoading = 0;
                this.options.ajax = (dataTablesParameters: any, callback) => {
                    this.request.setPagingData(dataTablesParameters);
                    this.options.api(this.request).subscribe(
                        resp => {
                            callback({
                                draw: dataTablesParameters.draw,
                                recordsTotal: resp.totalCount,
                                recordsFiltered: resp.totalCount,
                                data: resp.data,
                            });
                        },
                        error => {
                            callback({
                                draw: dataTablesParameters.draw,
                                recordsTotal: 0,
                                recordsFiltered: 0,
                                data: [],
                            });
                            // this.alertService.error(error);
                        }
                    );
                };
            }

            if (this.options.multiSelect) {
                this.options.select = {
                    style: 'multi',
                    selector: 'td:first-child'
                };

                this.options.columns.unshift({
                    title: '',
                    data: '',
                    width: '30px',
                    orderable: false,
                    className: 'select-checkbox'
                });
            }
        }

        this.table = $(this.tableElement.nativeElement);
        this.table.DataTable(this.options);

        this.refreshData();
    }

    private rebindEvent() {
        if (this.hasEdit) {
            this.hasAttachedListenerEdit = false;
        }
        if (this.hasDelete) {
            this.hasAttachedListenerDelete = false;
        }
        if (this.hasView) {
            this.hasAttachedListenerView = false;
        }
    }

    onAdd() {
        this.addClick.emit();
    }

    onDeleteMulti() {
        const items = this.table.DataTable().rows({ selected: true }).data().toArray();
        this.deleteClick.emit(items);
    }

    refreshData() {
        // Server data
        if (this.options.ajax) {
            this.table.DataTable().ajax.reload();
        } else {
            this.table.DataTable().clear().draw();
            this.table.DataTable().rows.add(this.options.data); // Add new data
            this.table.DataTable().columns.adjust().draw(); // Redraw the DataTable
        }
    }

    getRowData<T extends object | any[]>(ele): T {
        const tr = $(ele).closest('tr');
        return this.table.DataTable().row(tr).data() as T;
    }

    getRowsData<T extends object | any[]>(listIndex: number[]): T[] {
        return listIndex.map(i => this.table.DataTable().rows[i] as T);
    }

    search(searchModel: DataTableSearchModel[] | any) {
        if (this.options.serverSide) {
            this.request.setSearchData(searchModel);
            this.refreshData();
        } else {
            searchModel.forEach(element => {
                this.table.DataTable().columns(element.columnIndex).search(element.searchKey);
            });
            this.table.DataTable().draw();
        }
    }

    //#endregion
}
