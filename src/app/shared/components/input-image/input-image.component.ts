import { ChangeDetectorRef, Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

const noop = () => {
};

@Component({
    selector: 'common-input-image',
    templateUrl: './input-image.component.html',
    styleUrls: ['./input-image.component.scss'],
    providers: [
        { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => InputImageComponent), multi: true }
    ],
})
export class InputImageComponent implements OnInit, ControlValueAccessor {

    //#region Inputs, Outputs

    @Input() disabled: boolean = false;
    @Input() formClass: string = '';

    //#endregion

    //#region Properties

    fileInput: string | ArrayBuffer = 'assets/img/image_placeholder.jpg';

    private value: string | File;
    private onTouchedCallback: () => void = noop;
    private onChangeCallback: (_: any) => void = noop;

    //#endregion

    //#region Constructors

    constructor(private cd: ChangeDetectorRef) { }


    //#endregion

    //#region OnInit

    ngOnInit() {

    }

    //#endregion

    //#region Functions

    writeValue(obj: any): void {
        if (obj !== this.value) {
            this.value = obj;
            this.setImageView();
        }
    }

    registerOnChange(fn: any): void {
        this.onChangeCallback = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouchedCallback = fn;
    }

    setDisabledState?(isDisabled: boolean): void {
        throw new Error('Method not implemented.');
    }

    onFileInputChange(event) {
        if (event.target.files && event.target.files.length) {
            const file = event.target.files[0];
            this.value = file;
            this.onChangeCallback(this.value);
            this.setImageView();
        }
    }

    setImageView() {
        if (this.value) {
            if (typeof(this.value) === 'string'){
                this.fileInput = this.value;
            }
            else {
                const reader = new FileReader();

                reader.readAsDataURL(this.value);

                reader.onload = () => {
                    this.fileInput = reader.result;

                    // need to run CD since file load runs outside of zone
                    this.cd.markForCheck();
                };
            }
        }
        else {
            this.fileInput = 'assets/img/image_placeholder.jpg';
        }
    }

    //#endregion

}
