import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl, NgModel, FormControl, ValidationErrors } from '@angular/forms';

@Directive({
  // tslint:disable-next-line: directive-selector
  selector: '[formCompareTo]',
  providers: [{provide: NG_VALIDATORS, useExisting: CompareToDirective, multi: true}]
})
export class CompareToDirective implements Validator {

    @Input('formCompareTo') compareTo: any;

    validate(control: AbstractControl): ValidationErrors  | null {
        return control.value !== this.compareTo ? {'compare-error' : true} : null;
    }

}
