import { Directive, ElementRef, OnInit } from '@angular/core';

@Directive({
    selector: '[commonTree]'
})
export class TreeDirective implements OnInit {


    constructor(private element: ElementRef) {

    }

    ngOnInit(): void {
        $(this.element.nativeElement).tree();
    }
}
