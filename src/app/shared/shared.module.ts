import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DataTableComponent } from '@app/shared/components/data-table/data-table.component';
import { PopupComponent } from '@app/shared/components/popup/popup.component';
import { MainLayoutComponent } from '@app/shared/layouts/main-layout/main-layout.component';
import { FormsModule } from '@angular/forms';
import { TreeDirective } from './directives/tree.directive';
import { ChatBoxComponent } from './components/chat-box/chat-box.component';
import { ContextMenuModule } from 'ngx-contextmenu';
import { VirtualScrollerModule } from 'ngx-virtual-scroller';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { CompareToDirective } from './directives/compare-to.directive';
import { MainLayoutHeaderComponent } from '@app/shared/layouts/main-layout/header/main-layout-header.component';
import { MainLayoutFooterComponent } from '@app/shared/layouts/main-layout/footer/main-layout-footer.component';
import { MainLayoutPageHeaderComponent } from '@app/shared/layouts/main-layout/page-header/main-layout-page-header.component';
import { MainLayoutSidebarComponent } from '@app/shared/layouts/main-layout/sidebar/main-layout-sidebar.component';
import { MainLayoutSettingComponent } from '@app/shared/layouts/main-layout/setting/main-layout-setting.component';
import { InputImageComponent } from '@app/shared/components/input-image/input-image.component';

@NgModule({
    declarations: [
        DataTableComponent,
        PopupComponent,
        MainLayoutComponent,
        MainLayoutHeaderComponent,
        MainLayoutFooterComponent,
        MainLayoutPageHeaderComponent,
        MainLayoutSidebarComponent,
        MainLayoutSettingComponent,
        TreeDirective,
        ChatBoxComponent,
        CompareToDirective,
        InputImageComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        FormsModule,
        ContextMenuModule,
        VirtualScrollerModule,
        CKEditorModule,
    ],
    exports: [
        CommonModule,
        RouterModule,
        FormsModule,
        ContextMenuModule,
        VirtualScrollerModule,
        CKEditorModule,
        MainLayoutComponent,
        MainLayoutHeaderComponent,
        MainLayoutFooterComponent,
        MainLayoutPageHeaderComponent,
        MainLayoutSidebarComponent,
        MainLayoutSettingComponent,
        DataTableComponent,
        PopupComponent,
        ChatBoxComponent,
        CKEditorModule,
        CompareToDirective,
        InputImageComponent
    ]
})
export class SharedModule { }
