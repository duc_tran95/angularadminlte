import { Component, OnInit, HostBinding } from '@angular/core';
import { AdminLTEService } from '@app/core/services/common/admin-lte.service';
import { appConstants } from '@app/core/constants/appConstants';

@Component({
  // tslint:disable-next-line: component-selector
  selector: '[mainLayoutSetting]',
  templateUrl: './main-layout-setting.component.html',
  styleUrls: ['./main-layout-setting.component.scss']
})
export class MainLayoutSettingComponent implements OnInit {

  //#region Inputs

  @HostBinding('class') hostClass = 'control-sidebar control-sidebar-dark';

  //#endregion

  //#region Properties

  appConstants = appConstants;

  //#endregion

  //#region Constructors

  constructor(public adminLTEService: AdminLTEService) { }

  //#endregion

  //#region OnInit

  ngOnInit() {}

  //#endregion

  //#region Funtions

  //#endregion

}
