import { Component, OnInit, HostBinding } from '@angular/core';
import { SidebarItemModel } from '@app/core/models/data/sidebar-item.model';
import { AdminLTEService } from '@app/core/services/common/admin-lte.service';
import { Observable } from 'rxjs/internal/Observable';

declare var $;

@Component({
  // tslint:disable-next-line: component-selector
  selector: '[mainLayoutSidebar]',
  templateUrl: './main-layout-sidebar.component.html',
  styleUrls: ['./main-layout-sidebar.component.scss']
})
export class MainLayoutSidebarComponent implements OnInit {

  //#region Inputs

  @HostBinding('class') hostClass = 'main-sidebar sidebar-dark-primary elevation-4';

  //#endregion

  //#region Properties

  sidebarMenu$: Observable<SidebarItemModel[]>;

  //#endregion

  //#region Constructors

  constructor(public adminLTEService: AdminLTEService) {
    this.sidebarMenu$ = this.adminLTEService.sidebarMenu$;
   }

  //#endregion

  //#region OnInit

  ngOnInit() {

  }

  //#endregion

  //#region Funtions

  //#endregion

}
