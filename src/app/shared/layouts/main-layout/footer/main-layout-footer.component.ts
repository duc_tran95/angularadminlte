import { Component, OnInit, HostBinding } from '@angular/core';

@Component({
  // tslint:disable-next-line: component-selector
  selector: '[mainLayoutFooter]',
  templateUrl: './main-layout-footer.component.html',
  styleUrls: ['./main-layout-footer.component.scss']
})
export class MainLayoutFooterComponent implements OnInit {

  //#region Inputs, Outputs

  @HostBinding('class') hostClass = 'main-footer';

  //#endregion

  //#region Properties

  //#endregion

  //#region Constructors

  constructor() { }

  //#endregion

  //#region OnInit

  ngOnInit() {}

  //#endregion

  //#region Funtions

  //#endregion

}
