import { AdminLTEService } from '@app/core/services/common/admin-lte.service';
import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { ActivatedRoute, UrlSegment, Router, NavigationEnd } from '@angular/router';
import { SidebarItemModel, SidebarFlatItemModel } from '@app/core/models/data/sidebar-item.model';
import { combineLatest, Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

@Component({
    // tslint:disable-next-line: component-selector
    selector: '[mainLayoutPageHeader]',
    templateUrl: './main-layout-page-header.component.html',
    styleUrls: ['./main-layout-page-header.component.scss']
})
export class MainLayoutPageHeaderComponent implements OnInit {

    //#region Inputs

    @HostBinding('class') hostClass = 'content-header';

    //#endregion

    //#region Properties

    breadcrumbs: SidebarItemModel[];

    flattenSidebar$: Observable<SidebarFlatItemModel[]>;

    //#endregion

    //#region Constructors

    constructor(private route: ActivatedRoute, private router: Router, private adminLTEService: AdminLTEService) {
        this.flattenSidebar$ = this.adminLTEService.sidebarMenu$.pipe(
            map(sidebarMenu => {
                const stack = [];

                sidebarMenu.forEach((item, index) => {
                    stack.push({ data: item, parentIndex: -1, visited: false });
                });

                let i = 0;
                let currentItem = stack[0];

                while (currentItem) {
                    currentItem.visited = true;
                    if (currentItem.data.children) {
                        stack.push(...currentItem.data.children.map(item => ({ data: item, parentIndex: i, visited: false })));
                    }
                    currentItem = stack.find(x => !x.visited);
                    i++;
                }

                return stack;
            })
        );

        combineLatest([this.router.events.pipe(filter(event => event instanceof NavigationEnd)), this.flattenSidebar$]).subscribe(
            ([event, sidebarMenu]) => {
                const routerEvent = event as NavigationEnd;
                if (routerEvent.url && routerEvent.url.length > 0) {
                    this.getBreadcrumbs(routerEvent.url, sidebarMenu);
                }
            }
        );
    }

    //#endregion

    //#region OnInit

    ngOnInit() {
        this.breadcrumbs = [];
    }

    //#endregion

    //#region Funtions

    getBreadcrumbs(url: string, sidebarMenu: SidebarFlatItemModel[]) {
        this.breadcrumbs = [];

        if (url && url !== '/') {
            let menu = sidebarMenu.find(x => x.data.path === url);
            while (menu) {
                this.breadcrumbs.push(menu.data);
                menu = sidebarMenu[menu.parentIndex];
            }
        }

        this.breadcrumbs.push({
            name: 'Home',
            title: 'Home',
            path: '/'
        });

        this.breadcrumbs = this.breadcrumbs.reverse();
    }

    //#endregion

}
