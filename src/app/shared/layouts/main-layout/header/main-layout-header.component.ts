import { Component, OnInit, HostBinding } from '@angular/core';
import { AuthenticationService } from '@app/core/services/authentication/authentication.service';
import { UserModel } from '@app/core/models/data/user.model';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { AdminLTEService } from '@app/core/services/common/admin-lte.service';

@Component({
    // tslint:disable-next-line: component-selector
    selector: '[mainLayoutHeader]',
    templateUrl: './main-layout-header.component.html',
    styleUrls: ['./main-layout-header.component.scss']
})
export class MainLayoutHeaderComponent implements OnInit {

    //#region Inputs, Outputs

    @HostBinding('class') hostClass = 'main-header navbar navbar-expand navbar-white navbar-light';

    //#endregion

    //#region Properties

    user$: Observable<UserModel>;

    searchPage: string;

    //#endregion

    //#region Constructors

    constructor(public adminLTEService: AdminLTEService, private authService: AuthenticationService) {
        this.user$ = this.adminLTEService.user$;
    }

    //#endregion

    //#region OnInit

    ngOnInit() {

    }

    //#endregion

    //#region Funtions

    toggleNotificationMenu() {
        $('#headerNotificationDropdown').dropdown('toggle');
    }

    toggleUserMenu() {
        $('#headerUserDropdown').dropdown('toggle');
    }

    logOut() {
        this.authService.logOut();
        location.reload();
    }

    onSearchFormSubmit(searchPageForm: NgForm) {
        if (searchPageForm.valid) {
            this.searchPage.trim();
        }
    }

    //#endregion

}
