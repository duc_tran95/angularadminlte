import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AdminLTEService } from '@app/core/services/common/admin-lte.service';

@Component({
    // tslint:disable-next-line: component-selector
    selector: '[mainLayout]',
    templateUrl: './main-layout.component.html',
    styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit, AfterViewInit {

    //#region Inputs, Outputs

    //#endregion

    //#region Properties

    //#endregion

    //#region Constructors

    constructor(private adminLTEService: AdminLTEService) { }

    //#endregion

    //#region OnInit

    ngOnInit() {
        this.adminLTEService.getSidebarMenu();
    }

    ngAfterViewInit() {
        this.adminLTEService.setAdminLTEStyle();
    }

    //#endregion

    //#region Funtions

    //#endregion

}
