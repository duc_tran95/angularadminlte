import { CityModel } from '@app/core/models/data/city.model';
import { PagingRequest } from '@app/core/models/api-data/base.request';

export class CityRequest {
    constructor(model: CityModel) {
        this.id = model.id;
        this.name = model.name;
    }

    id: number;
    name: string;
}

export class CityPagingRequest extends PagingRequest {
    constructor() {
        super();
        this.name = '';
    }

    name: string;

    getRequestField(modelField: string): string {
        switch (modelField) {
            default: return modelField;
        }
    }
}
