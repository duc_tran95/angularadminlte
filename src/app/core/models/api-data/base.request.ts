import { HttpParams } from '@angular/common/http';
import { DataTableParameter } from '@app/core/models/data/base.model';

export class PagingRequest {
    sort: string;
    isDescending: boolean;
    page: number;
    pageSize: number;

    toHttpParams(): HttpParams {
        let params = new HttpParams();
        Object.keys(this).forEach(key => {
            params = params.set(key, this[key]);
        });
        return params;
    }

    setSearchData(search: any) {
        Object.keys(search).forEach(key => {
            this[this.getRequestField(key)] = search[key] || '';
        });
    }

    setPagingData(dataTablesParameters: DataTableParameter) {
        const sortColumn = dataTablesParameters.order[0];
        if (sortColumn) {
            this.isDescending = sortColumn.dir === 'desc';
            this.sort = this.getRequestField(dataTablesParameters.columns[sortColumn.column].data);
        }
        this.pageSize = dataTablesParameters.length;
        this.page = dataTablesParameters.start / dataTablesParameters.length + 1;

    }

    getRequestField(modelField: string): string {
        return modelField;
    }
}
