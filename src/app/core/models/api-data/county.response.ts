export class CountyResponse {
    id: number;
    name: string;
    cityId: number;
}
