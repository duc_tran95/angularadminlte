export class BaseResponse<T> {
    statusCode: number;
    statusText: string;
    data: T;
}

export class PagingResponse<T> extends BaseResponse<PagingResponseData<T>> {
}

export class PagingResponseData<T> {
    page: number;
    pageSize: number;
    totalCount: number;
    totalPages: number;
    hasPreviousPage: boolean;
    hasNextPage: boolean;
    data: T[];
}



