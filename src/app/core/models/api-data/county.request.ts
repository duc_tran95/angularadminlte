import { CountyModel } from '@app/core/models/data/county.model';
import { PagingRequest } from '@app/core/models/api-data/base.request';

export class CountyRequest {
    constructor(model: CountyModel) {
        this.id = model.id;
        this.name = model.name;
        this.id = model.city.id;
    }

    id: number;
    name: string;
    cityId: number;
}

export class CountyPagingRequest extends PagingRequest {
    constructor() {
        super();
        this.name = '';
        this.cityId = null;
    }

    name: string;
    cityId: number;

    getRequestField(modelField: string): string {
        switch (modelField) {
            default: return modelField;
        }
    }
}
