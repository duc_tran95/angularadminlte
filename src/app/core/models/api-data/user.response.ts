export class UserResponse {
    id: number;
    userName: string;
    fullName: string;
    email: string;
    address: string;
    phoneNumber: string;
}
