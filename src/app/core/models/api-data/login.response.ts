import { UserResponse } from '@app/core/models/api-data/user.response';

export class LoginResponse {
    token: string;
    user: UserResponse;
}
