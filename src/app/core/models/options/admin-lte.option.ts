export class AdminLTEOption {
    constructor(data: Partial<AdminLTEOption>) {
        Object.assign(this, data);
    }

    noNavbarBorder: boolean = false;
    bodySmallText: boolean = false;
    navbarSmallText: boolean = false;
    sidebarSmallText: boolean = false;
    footerSmallText: boolean = false;
    sidebarFlatStyle: boolean = false;
    sidebarLegacyStyle: boolean = false;
    sidebarCompact: boolean = false;
    sidebarChildIndent: boolean = false;
    sidebarDisableHoverAutoExpand: boolean = false;
    brandSmallText: boolean = false;

    navbarVariant: string = 'white';
    accentColorVariant: string = '';
    darkSidebarVariant: string = 'primary';
    lightSidebarVariant: string = '';
    brandLogoVariant: string = '';

}
