import { Observable } from 'rxjs';
import { PagingModel } from '@app/core/models/data/base.model';
import { PagingRequest } from '@app/core/models/api-data/base.request';

export class DataTableOption implements DataTables.Settings {
    constructor(data: Partial<DataTableOption>) {
        Object.assign(this, data);
    }

    multiSelect: boolean = false;
    data: any[] = [];
    columns: DataTables.ColumnSettings[] = [];
    columnDefs: DataTables.ColumnDefsSettings[] = [];
    paging: boolean = true;
    pageLength: number = 10;
    lengthMenu: number[] = [10, 20, 50];
    lengthChange: boolean = false;
    scrollX: boolean = false;
    scrollY: string = null;
    searching: boolean = false;
    ordering: boolean = true;
    order: Array<(number | string)> | Array<Array<(number | string)>> = [];
    info: boolean = true;
    autoWidth: boolean = false;
    ajax: any = null;
    drawCallback: any = null;
    actions: ('Add' | 'Edit' | 'Delete' | 'View')[] = [];
    dom: string = 'lrtip';
    deferLoading: number = null;
    processing: boolean = true;
    serverSide: boolean = false;
    createdRow: any = null;
    pagingType: 'numbers' | 'simple' |'simple_numbers' | 'full' | 'full_numbers' | 'first_last_numbers' = 'simple_numbers';
    api: (request: PagingRequest) => Observable<PagingModel<any>> = null;
    select: {
        style: string;
        selector: string;
    } = null;
}
