export class ToastrOption implements ToastrOptions {
    constructor(data: Partial<ToastrOptions>) {
        Object.assign(this, data);
    }

    escapeHtml: boolean = false;
    closeButton: boolean = true;
    progressBar: boolean = false;
    positionClass: string = 'toast-top-right';
    preventDuplicates: boolean = false;
    onclick: any = null;
    newestOnTop: boolean = false;
    showDuration: number = 1000;
    hideDuration: number = 1000;
    extendedTimeOut: number = 5000;
    showEasing: string = 'swing';
    hideEasing: string = 'linear';
    showMethod: string = 'fadeIn';
    hideMethod: string =  'fadeOut';
}
