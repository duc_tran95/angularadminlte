export class PopupOption {
    constructor(data: Partial<PopupOption>) {
        Object.assign(this, data);
    }

    type: '' | 'primary' | 'info' | 'warning' | 'success' | 'danger' = '';
    size: '' | 'sm' | 'lg' | 'xl' = '';
    title: string;
    cancelText: string = 'Cancel';
    okText: string = 'OK';
}
