export class LoginModel {
    constructor() {
        this.email = '';
        this.password = '';
        this.remember = false;
    }

    email: string;
    password: string;
    remember: boolean;
}
