import { PagingResponseData } from '@app/core/models/api-data/base.response';

export class BaseUserModel {
    constructor() {
        this.id = null;
        this.email = null;
        this.userName = null;
        this.type = null;
    }

    id?: string;
    email?: string;
    userName?: string;
    type?: 'you' | 'me';
}

export class PagingModel<T> {
    constructor(resp: PagingResponseData<any>) {
        this.pageSize = resp.pageSize;
        this.totalPages = resp.totalPages;
        this.totalCount = resp.totalCount;
        this.currentPage = resp.page;
    }

    currentPage: number;
    totalPages: number;
    totalCount: number;
    pageSize: number;
    data: T[];
}

export class DataTableParameter {
    draw: number;
    length: number;
    start: number;
    columns: {
        data: string;
        name: string;
        orderable: boolean;
        searchable: boolean;
        search: {
            regex: boolean;
            value: string;
        }
    }[];
    order: {
        column: number;
        dir: 'asc'|'desc';
    }[];
    search: {
        regex: boolean;
        value: string;
    };
}

export class FlattenItem<T> {
    data: T;
    parentIndex: number;
    visited: boolean;
}
