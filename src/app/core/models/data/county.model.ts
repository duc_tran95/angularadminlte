import { CityModel } from '@app/core/models/data/city.model';
import { CountyResponse } from '@app/core/models/api-data/county.response';

export class CountyModel {
    constructor();
    constructor(response: CountyResponse);
    constructor(response?: CountyResponse) {
        this.id = response ? response.id : null;
        this.name = response ? response.name : '';
        this.city = {
            id: response ? response.cityId : null,
            name: ''
        };
    }

    id: number;
    name: string;
    city: CityModel;
}
