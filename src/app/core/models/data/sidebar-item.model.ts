import { FlattenItem } from '@app/core/models/data/base.model';

export class SidebarItemModel {
    name: string;
    path: string;
    title?: string;
    icon?: string;
    children?: SidebarItemModel[];
}

export class SidebarFlatItemModel extends FlattenItem<SidebarItemModel> {

}
