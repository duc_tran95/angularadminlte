import { CityResponse } from '@app/core/models/api-data/city.response';

export class CityModel {
    constructor();
    constructor(response: CityResponse);
    constructor(response?: CityResponse) {
        this.id = response ? response.id : null;
        this.name = response ? response.name : '';
    }

    id: number;
    name: string;
}
