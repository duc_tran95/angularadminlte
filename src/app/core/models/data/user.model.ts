import { UserResponse } from '@app/core/models/api-data/user.response';

export class UserModel {
    constructor();
    constructor(response: UserResponse);
    constructor(response?: UserResponse) {
        this.id = response ? response.id : null;
        this.userName = response ? response.userName : '';
        this.fullName = response ? response.fullName : '';
        this.email = response ? response.email : '';
        this.address = response ? response.address : '';
        this.phoneNumber = response ? response.phoneNumber : '';
        this.password = '';
    }

    id: number;
    userName: string;
    fullName: string;
    password: string;
    email: string;
    address: string;
    phoneNumber: string;
}
