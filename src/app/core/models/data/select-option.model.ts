export class SelectOptionModel {
    constructor(value?: any, text?: any) {
        this.value = value || null;
        this.text = text || null;
    }

    value: any;
    text: string;
}
