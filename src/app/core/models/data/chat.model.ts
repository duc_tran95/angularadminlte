import {BaseUserModel} from '@app/core/models/data/base.model';

export class ChatModel {
    id: string;
    content: string;
    timeSend: Date;
    user: BaseUserModel;
}
