import { Injectable } from '@angular/core';
import { LoginModel } from '@app/core/models/data/login.model';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';
import { apiUrls } from '@app/core/constants/apiUrls';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { LocalStorageService } from '@app/core/services/common/local-storage.service';
import { LoginRequest } from '@app/core/models/api-data/login.request';
import { LoginResponse } from '@app/core/models/api-data/login.response';
import { AlertService } from '@app/core/services/common/alert.service';
import { AdminLTEService } from '@app/core/services/common/admin-lte.service';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {

    //#region Inputs, Outputs

    //#endregion

    //#region Properties

    private baseUrl = environment.baseUrl;
    private apiUrl = apiUrls.user;
    private jwtSecret: string;

    //#endregion

    //#region Constructors

    constructor(private http: HttpClient, private localStorageService: LocalStorageService,
                private alertService: AlertService, private adminLTEService: AdminLTEService) {
        const jwtSecret = this.localStorageService.getLocal('jwtSecret');
        if (jwtSecret) {
            this.jwtSecret = jwtSecret;
        }
    }

    //#endregion

    //#region Funtions

    logIn(loginParam: LoginModel): Observable<boolean> {
        const requestParams = new LoginRequest(loginParam);
        return this.http.post(this.baseUrl + this.apiUrl.login, requestParams).pipe(
            map(
                (resp: LoginResponse) => {
                    if (resp.token) {
                        this.jwtSecret = resp.token;
                        if (loginParam.remember) {
                            this.localStorageService.saveLocal('_jwtSecret', this.jwtSecret);
                        }
                        return true;
                    } else {
                        this.jwtSecret = null;
                        this.alertService.error('Login failed');
                        throw Error('Login failed');
                    }
                }
            )
        );
    }

    logOut() {
        this.jwtSecret = null;
        this.localStorageService.clearLocal('jwtSecret');
    }

    isAuthenticated(): boolean {
        return this.jwtSecret != null;
    }

    get jwt(): string {
        return this.jwtSecret;
    }

    //#endregion
}
