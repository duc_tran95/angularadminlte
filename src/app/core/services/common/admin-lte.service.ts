import { Injectable } from '@angular/core';
import { LayoutService } from '@app/core/services/common/layout.service';
import { SidebarItemModel } from '@app/core/models/data/sidebar-item.model';
import { Subject, BehaviorSubject } from 'rxjs';
import { UserModel } from '@app/core/models/data/user.model';
import { UserService } from '@app/core/services/data/user.service';
import { AdminLTEOption } from '@app/core/models/options/admin-lte.option';
import { LocalStorageService } from '@app/core/services/common/local-storage.service';
import { appConstants } from '@app/core/constants/appConstants';

@Injectable({
    providedIn: 'root'
})
export class AdminLTEService {

    //#region Properties

    private sidebarMenuData: Subject<SidebarItemModel[]> = new BehaviorSubject<SidebarItemModel[]>([]);
    sidebarMenu$ = this.sidebarMenuData.asObservable();

    private userData: Subject<UserModel> = new BehaviorSubject<UserModel>(new UserModel());
    user$ = this.userData.asObservable();

    adminLTEOption = new AdminLTEOption({});

    //#endregion

    //#region Constructors

    constructor(private layoutService: LayoutService, private userService: UserService,
                private localStorageService: LocalStorageService) {

        this.getAdminLTEOptionFromCache();
    }

    //#endregion

    //#region Funtions

    getSidebarMenu() {
        this.layoutService.getSidebarMenu().subscribe(
            resp => {
                this.sidebarMenuData.next(resp);
            }
        );
    }

    getUser() {
        this.userService.getCurrentUser().subscribe(
            resp => {
                this.userData.next(resp);
            }
        );
    }

    //#region Admin LTE style

    setAdminLTEStyle() {
        this.setNoNavbarBorder(false);
        this.setBodySmallText(false);
        this.setNavbarSmallText(false);
        this.setSidebarSmallText(false);
        this.setFooterSmallText(false);
        this.setSidebarFlatStyle(false);
        this.setSidebarLegacyStyle(false);
        this.setSidebarCompact(false);
        this.setSidebarChildIndent(false);
        this.setSidebarDisableHoverAutoExpand(false);
        this.setBrandSmallText(false);
        this.setNavbarVariant(false);
        this.setAccentColorVariant(false);
        this.setDarkSidebarVariant(false);
        this.setLightSidebarVariant(false);
        this.setBrandLogoVariant(false);
    }

    setNoNavbarBorder(save: boolean = true) {
        if (this.adminLTEOption.noNavbarBorder) {
            $('.main-header').addClass('border-bottom-0');
        } else {
            $('.main-header').removeClass('border-bottom-0');
        }

        if (save) {
            this.saveAdminLTEOptionToCache();
        }
    }

    setBodySmallText(save: boolean = true) {
        if (this.adminLTEOption.bodySmallText) {
            $('body').addClass('text-sm');
        } else {
            $('body').removeClass('text-sm');
        }

        if (save) {
            this.saveAdminLTEOptionToCache();
        }
    }

    setNavbarSmallText(save: boolean = true) {
        if (this.adminLTEOption.navbarSmallText) {
            $('.main-header').addClass('text-sm');
        } else {
            $('.main-header').removeClass('text-sm');
        }

        if (save) {
            this.saveAdminLTEOptionToCache();
        }
    }

    setSidebarSmallText(save: boolean = true) {
        if (this.adminLTEOption.sidebarSmallText) {
            $('.nav-sidebar').addClass('text-sm');
        } else {
            $('.nav-sidebar').addClass('text-sm');
        }

        if (save) {
            this.saveAdminLTEOptionToCache();
        }
    }

    setFooterSmallText(save: boolean = true) {
        if (this.adminLTEOption.footerSmallText) {
            $('.main-footer').addClass('text-sm');
        } else {
            $('.main-footer').removeClass('text-sm');
        }

        if (save) {
            this.saveAdminLTEOptionToCache();
        }
    }

    setSidebarFlatStyle(save: boolean = true) {
        if (this.adminLTEOption.sidebarFlatStyle) {
            $('.nav-sidebar').addClass('nav-flat');
        } else {
            $('.nav-sidebar').removeClass('nav-flat');
        }

        if (save) {
            this.saveAdminLTEOptionToCache();
        }
    }

    setSidebarLegacyStyle(save: boolean = true) {
        if (this.adminLTEOption.sidebarLegacyStyle) {
            $('.nav-sidebar').addClass('nav-legacy');
        } else {
            $('.nav-sidebar').removeClass('nav-legacy');
        }

        if (save) {
            this.saveAdminLTEOptionToCache();
        }
    }

    setSidebarCompact(save: boolean = true) {
        if (this.adminLTEOption.sidebarCompact) {
            $('.nav-sidebar').addClass('nav-compact');
        } else {
            $('.nav-sidebar').removeClass('nav-compact');
        }

        if (save) {
            this.saveAdminLTEOptionToCache();
        }
    }

    setSidebarChildIndent(save: boolean = true) {
        if (this.adminLTEOption.sidebarChildIndent) {
            $('.nav-sidebar').addClass('nav-child-indent');
        } else {
            $('.nav-sidebar').removeClass('nav-child-indent');
        }

        if (save) {
            this.saveAdminLTEOptionToCache();
        }
    }

    setSidebarDisableHoverAutoExpand(save: boolean = true) {
        if (this.adminLTEOption.sidebarDisableHoverAutoExpand) {
            $('.main-sidebar').addClass('sidebar-no-expand');
        } else {
            $('.main-sidebar').removeClass('sidebar-no-expand');
        }

        if (save) {
            this.saveAdminLTEOptionToCache();
        }
    }

    setBrandSmallText(save: boolean = true) {
        if (this.adminLTEOption.brandSmallText) {
            $('.brand-link').addClass('text-sm');
        } else {
            $('.brand-link').removeClass('text-sm');
        }

        if (save) {
            this.saveAdminLTEOptionToCache();
        }
    }

    setNavbarVariant(save: boolean = true) {
        const $mainHeader = $('.main-header');
        const color = `navbar-${this.adminLTEOption.navbarVariant}`;

        $mainHeader.removeClass('navbar-dark').removeClass('navbar-light');

        appConstants.adminLTE.navbarVariants.forEach(item => {
            $mainHeader.removeClass(`navbar-${item}`);
        });

        if (appConstants.adminLTE.lightVariants.indexOf(this.adminLTEOption.navbarVariant) > -1) {
            $mainHeader.addClass('navbar-light');
        } else {
            $mainHeader.addClass('navbar-dark');
        }

        $mainHeader.addClass(color);

        if (save) {
            this.saveAdminLTEOptionToCache();
        }
    }

    setAccentColorVariant(save: boolean = true) {
        const color = `accent-${this.adminLTEOption.accentColorVariant}`;
        const $body = $('body');

        appConstants.adminLTE.colorVariants.forEach(item => {
            $body.removeClass(`accent-${item}`);
        });

        $body.addClass(color);

        if (save) {
            this.saveAdminLTEOptionToCache();
        }
    }

    setDarkSidebarVariant(save: boolean = true) {
        if (this.adminLTEOption.darkSidebarVariant) {
            const color = `sidebar-dark-${this.adminLTEOption.darkSidebarVariant}`;
            const $sidebar = $('.main-sidebar');

            appConstants.adminLTE.colorVariants.forEach(item => {
                $sidebar.removeClass(`sidebar-dark-${item}`);
                $sidebar.removeClass(`sidebar-light-${item}`);
            });

            $sidebar.addClass(color);

            this.adminLTEOption.lightSidebarVariant = '';
            this.setLightSidebarVariant();
        }

        if (save) {
            this.saveAdminLTEOptionToCache();
        }
    }

    setLightSidebarVariant(save: boolean = true) {
        if (this.adminLTEOption.lightSidebarVariant) {
            const color = `sidebar-light-${this.adminLTEOption.lightSidebarVariant}`;
            const $sidebar = $('.main-sidebar');

            appConstants.adminLTE.colorVariants.forEach(item => {
                $sidebar.removeClass(`sidebar-dark-${item}`);
                $sidebar.removeClass(`sidebar-light-${item}`);
            });

            $sidebar.addClass(color);

            this.adminLTEOption.darkSidebarVariant = '';
            this.setDarkSidebarVariant();
        }

        if (save) {
            this.saveAdminLTEOptionToCache();
        }
    }

    setBrandLogoVariant(save: boolean = true) {
        const color = `navbar-${this.adminLTEOption.brandLogoVariant}`;
        const $logo = $('.brand-link');

        appConstants.adminLTE.navbarVariants.forEach(item => {
            $logo.removeClass(`navbar-${item}`);
        });

        $logo.addClass(color);

        if (save) {
            this.saveAdminLTEOptionToCache();
        }
    }

    getAdminLTEOptionFromCache() {
        this.adminLTEOption = new AdminLTEOption(this.localStorageService.getLocal('adminLTEOption'));
    }

    saveAdminLTEOptionToCache() {
        this.localStorageService.saveLocal('adminLTEOption', this.adminLTEOption);
    }

    //#endregion

    //#endregion

}
