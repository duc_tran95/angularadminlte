import { Injectable } from '@angular/core';
import 'toastr';
import { ToastrOption } from '@app/core/models/options/toastr.option';
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  private readonly defaultOption: ToastrOption = new ToastrOption({});

  constructor() { }

  success(data: string, option: ToastrOption = null) {
     const newOption = _.assign({}, this.defaultOption, option);
     toastr.success(data, null, newOption);
  }

  info(data: string, option: ToastrOption = null) {
    const newOption = _.assign({}, this.defaultOption, option);
    toastr.info(data, null, newOption);
  }

  warning(data: string, option: ToastrOption = null) {
    const newOption = _.assign({}, this.defaultOption, option);
    toastr.warning(data, null, newOption);
  }

  error(data: string, option: ToastrOption = null) {
    const newOption = _.assign({}, this.defaultOption, option);
    toastr.error(data, null, newOption);
  }

  clear() {
    toastr.clear();
  }
}
