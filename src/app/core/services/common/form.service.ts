import {Injectable} from '@angular/core';
import {NgModel, NgForm} from '@angular/forms';

@Injectable({
    providedIn: 'root'
})
export class FormService {

    constructor() {
    }

    getFormControlClass(form: NgForm, model: NgModel | boolean): string {
        let unset = false;
        let valid = false;
        let invalid = false;
        if (typeof model === 'object') {
            unset = (model.pristine && !form.submitted);
            valid = model.valid && (model.dirty || form.submitted);
            invalid = model.invalid && (model.dirty || form.submitted);
        } else {
            valid = model || !form.submitted;
            invalid = !model && form.submitted;
        }

        if (valid) {
            return 'is-valid';
        }
        if (invalid) {
            return 'is-invalid';
        }

        return '';
    }

    trimSpaces(data: any) {
        Object.keys(data).forEach(key => {
            const prop = data[key];
            if (prop && typeof prop == 'string') {
                data[key] = prop.trim();
            }
        });
    }
}
