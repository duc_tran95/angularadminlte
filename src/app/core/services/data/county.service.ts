import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { apiUrls } from '@app/core/constants/apiUrls';
import { CountyModel } from '@app/core/models/data/county.model';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';
import { CountyResponse } from '@app/core/models/api-data/county.response';
import { map } from 'rxjs/operators';
import * as _ from 'lodash';
import { CountyRequest, CountyPagingRequest } from '@app/core/models/api-data/county.request';
import { PagingModel } from '@app/core/models/data/base.model';
import { PagingResponseData } from '@app/core/models/api-data/base.response';

@Injectable({
    providedIn: 'root'
})
export class CountyService {

    //#region Properties

    private baseUrl = environment.baseUrl;
    private apiUrl = apiUrls.county;

    //#endregion

    //#region Constructors

    constructor(private http: HttpClient) { }

    //#endregion

    //#region Funtions

    getAll(): Observable<CountyModel[]> {
        return this.http.get(this.baseUrl + this.apiUrl.getAll).pipe(
            map(
                (resp: CountyResponse[]) => {
                    return _.map(resp, item => new CountyModel(item));
                }
            )
        );
    }

    getByPaging(request: CountyPagingRequest): Observable<PagingModel<CountyModel>> {
        return this.http.post(this.baseUrl + this.apiUrl.getByPaging, { params: request.toHttpParams() }).pipe(
            map(
                (resp: PagingResponseData<CountyResponse>) => {
                    const pagingData = new PagingModel<CountyModel>(resp);
                    pagingData.data = _.map(resp.data, item => new CountyModel(item));
                    return pagingData;
                }
            )
        );
    }

    getById(id: number): Observable<CountyModel> {
        const data = new HttpParams().append('id', id.toString());
        return this.http.get(this.baseUrl + this.apiUrl.getById, { params: data }).pipe(
            map(
                (resp: CountyResponse) => {
                    return new CountyModel(resp);
                }
            )
        );
    }

    add(county: CountyModel) {
        const requestParams = new CountyRequest(county);
        return this.http.post(this.baseUrl + this.apiUrl.add, requestParams).pipe(
            map(
                (resp: boolean) => {
                    return resp;
                }
            )
        );
    }

    update(county: CountyModel) {
        const requestParams = new CountyRequest(county);
        return this.http.post(this.baseUrl + this.apiUrl.update, requestParams).pipe(
            map(
                (resp: boolean) => {
                    return resp;
                }
            )
        );
    }

    delete(id: number) {
        const data = new HttpParams().append('id', id.toString());
        return this.http.delete(this.baseUrl + this.apiUrl.delete, { params: data }).pipe(
            map(
                (resp: boolean) => {
                    return resp;
                }
            )
        );
    }

    //#endregion
}
