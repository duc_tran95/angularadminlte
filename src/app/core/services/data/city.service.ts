import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { apiUrls } from '@app/core/constants/apiUrls';
import { CityModel } from '@app/core/models/data/city.model';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';
import { CityResponse } from '@app/core/models/api-data/city.response';
import { map } from 'rxjs/operators';
import * as _ from 'lodash';
import { CityRequest, CityPagingRequest } from '@app/core/models/api-data/city.request';
import { PagingModel } from '@app/core/models/data/base.model';
import { PagingResponseData } from '@app/core/models/api-data/base.response';

@Injectable({
    providedIn: 'root'
})
export class CityService {

    //#region Properties

    private baseUrl = environment.baseUrl;
    private apiUrl = apiUrls.city;

    //#endregion

    //#region Constructors

    constructor(private http: HttpClient) { }

    //#endregion

    //#region Funtions

    getAll(): Observable<CityModel[]> {
        return this.http.get(this.baseUrl + this.apiUrl.getAll).pipe(
            map(
                (resp: CityResponse[]) => {
                    return _.map(resp, item => new CityModel(item));
                }
            )
        );
    }

    getByPaging(request: CityPagingRequest): Observable<PagingModel<CityModel>> {
        return this.http.post(this.baseUrl + this.apiUrl.getByPaging, { params: request.toHttpParams() }).pipe(
            map(
                (resp: PagingResponseData<CityResponse>) => {
                    const pagingData = new PagingModel<CityModel>(resp);
                    pagingData.data = _.map(resp.data, item => new CityModel(item));
                    return pagingData;
                }
            )
        );
    }

    getById(id: number): Observable<CityModel> {
        const data = new HttpParams().append('id', id.toString());
        return this.http.get(this.baseUrl + this.apiUrl.getById, { params: data }).pipe(
            map(
                (resp: CityResponse) => {
                    return new CityModel(resp);
                }
            )
        );
    }

    add(city: CityModel) {
        const requestParams = new CityRequest(city);
        return this.http.post(this.baseUrl + this.apiUrl.add, requestParams).pipe(
            map(
                (resp: boolean) => {
                    return resp;
                }
            )
        );
    }

    update(city: CityModel) {
        const requestParams = new CityRequest(city);
        return this.http.post(this.baseUrl + this.apiUrl.update, requestParams).pipe(
            map(
                (resp: boolean) => {
                    return resp;
                }
            )
        );
    }

    delete(id: number) {
        const data = new HttpParams().append('id', id.toString());
        return this.http.delete(this.baseUrl + this.apiUrl.delete, { params: data }).pipe(
            map(
                (resp: boolean) => {
                    return resp;
                }
            )
        );
    }

    //#endregion
}
