import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { apiUrls } from '@app/core/constants/apiUrls';
import { UserModel } from '@app/core/models/data/user.model';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';
import { UserResponse } from '@app/core/models/api-data/user.response';
import { map } from 'rxjs/operators';
import * as _ from 'lodash';
import { UserRequest, UserPagingRequest } from '@app/core/models/api-data/user.request';
import { PagingModel } from '@app/core/models/data/base.model';
import { PagingResponseData } from '@app/core/models/api-data/base.response';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    //#region Properties

    private baseUrl = environment.baseUrl;
    private apiUrl = apiUrls.user;

    //#endregion

    //#region Constructors

    constructor(private http: HttpClient) { }

    //#endregion

    //#region Funtions

    getCurrentUser(): Observable<UserModel> {
        return this.http.get(this.baseUrl + this.apiUrl.getCurrentUser).pipe(
            map(
                (resp: UserResponse) => {
                    return new UserModel(resp);
                }
            )
        );
    }

    getAll(): Observable<UserModel[]> {
        return this.http.get(this.baseUrl + this.apiUrl.getAll).pipe(
            map(
                (resp: UserResponse[]) => {
                    return _.map(resp, item => new UserModel(item));
                }
            )
        );
    }

    getByPaging(request: UserPagingRequest): Observable<PagingModel<UserModel>> {
        return this.http.post(this.baseUrl + this.apiUrl.getByPaging, { params: request.toHttpParams() }).pipe(
            map(
                (resp: PagingResponseData<UserResponse>) => {
                    const pagingData = new PagingModel<UserModel>(resp);
                    pagingData.data = _.map(resp.data, item => new UserModel(item));
                    return pagingData;
                }
            )
        );
    }

    getById(id: number): Observable<UserModel> {
        const data = new HttpParams().append('id', id.toString());
        return this.http.get(this.baseUrl + this.apiUrl.getById, { params: data }).pipe(
            map(
                (resp: UserResponse) => {
                    return new UserModel(resp);
                }
            )
        );
    }

    add(user: UserModel) {
        const requestParams = new UserRequest(user);
        return this.http.post(this.baseUrl + this.apiUrl.add, requestParams).pipe(
            map(
                (resp: boolean) => {
                    return resp;
                }
            )
        );
    }

    update(user: UserModel) {
        const requestParams = new UserRequest(user);
        return this.http.post(this.baseUrl + this.apiUrl.update, requestParams).pipe(
            map(
                (resp: boolean) => {
                    return resp;
                }
            )
        );
    }

    delete(id: number) {
        const data = new HttpParams().append('id', id.toString());
        return this.http.delete(this.baseUrl + this.apiUrl.delete, { params: data }).pipe(
            map(
                (resp: boolean) => {
                    return resp;
                }
            )
        );
    }

    //#endregion
}
