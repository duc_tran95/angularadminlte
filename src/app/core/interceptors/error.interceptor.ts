import { Injectable } from '@angular/core';
import {
    HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse, HttpResponse
} from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { AlertService } from '@app/core/services/common/alert.service';
import { BaseResponse } from '@app/core/models/api-data/base.response';


@Injectable({
    providedIn: 'root'
})


export class ErrorInterceptor implements HttpInterceptor {

    //#region Properties

    //#endregion

    //#region Constructors

    constructor(private alertService: AlertService) { }

    //#endregion

    //#region Funtions

    intercept(req: HttpRequest<any>, next: HttpHandler) {
        return next.handle(req).pipe(
            map(
                event => {
                    if (event instanceof HttpResponse && event.body && event.body.statusCode) {
                        const body = event.body as BaseResponse<any>;

                        if (body.statusCode !== 200) {
                            throw Error(body.statusText);
                        }

                        const modEvent = event.clone({ body: body.data });
                        return modEvent;
                    }
                    return event;
                }
            ),
            catchError(err => {
                if (err instanceof HttpErrorResponse) {
                    this.alertService.error(err.statusText);
                    throw Error(err.statusText);
                } else if (err instanceof Error) {
                    this.alertService.error(err.message);
                    throw Error(err.message);
                }

                this.alertService.error('Internal Server Error!');
                throw Error('Internal Server Error!');
            })
        );
    }

    //#endregion
}
