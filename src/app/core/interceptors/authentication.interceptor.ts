import { Injectable } from '@angular/core';
import {
    HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders
} from '@angular/common/http';
import { AuthenticationService } from '@app/core/services/authentication/authentication.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';


@Injectable({
    providedIn: 'root'
})


export class AuthenticationInterceptor implements HttpInterceptor {

    //#region Properties

    //#endregion

    //#region Constructors

    constructor(private authService: AuthenticationService, private spinner: NgxSpinnerService) { }

    //#endregion

    //#region Funtions

    intercept(req: HttpRequest<any>, next: HttpHandler) {
        const header = {
            'Content-Type': 'application/json',
        };

        if (this.authService.jwt) {
            // tslint:disable-next-line: no-string-literal
            header['Authorization'] = 'Bearer ' + this.authService.jwt;
        }

        req = req.clone({
            setHeaders: header
        });

        this.spinner.show();
        return next.handle(req).pipe(
            finalize(() => this.spinner.hide())
        );
    }

    //#endregion
}
