export const appConstants = {
    dateTimeFormat: 'dd/MM/yyyy hh:mm:ss a',
    dateInputFormat: 'yyyy-MM-dd',
    dateFormatMoment: 'DD/MM/YYYY',
    passwordPattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*_=+-]).{8,}$/,
    emailPattern: /^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$/,
    phoneNumberPattern: /^[+0][0-9]{8,10}$/,
    datePickerConfig: {
        dateInputFormat: 'DD/MM/YYYY'
    },
    kendo: {
        dropdown: {
            defaultItem: { text: ' -- Select -- ', value: null },
            virtual: {
                itemHeight: 36,
                pageSize: 20
            }
        }
    },
    adminLTE: {
        colorVariants: [
            'primary',
            'warning',
            'info',
            'danger',
            'success',
            'indigo',
            'lightblue',
            'navy',
            'purple',
            'fuchsia',
            'pink',
            'maroon',
            'orange',
            'lime',
            'teal',
            'olive'
        ],
        navbarVariants: [
            'primary',
            'secondary',
            'info',
            'success',
            'danger',
            'indigo',
            'purple',
            'pink',
            'navy',
            'lightblue',
            'teal',
            'cyan',
            'dark',
            'gray-dark',
            'gray',
            'light',
            'warning',
            'white',
            'orange',
        ],
        lightVariants: [
            '',
            'light',
            'warning',
            'white',
            'orange',
        ]
    }
};
