export const apiUrls = {
    layout: {
        getSidebarMenu: '/assets/data/sidebar-menu.json',
    },
    user: {
        login: '/account/login',
        getCurrentUser: '/account/getCurrentUser',
        getAll: '/account/getall',
        getByPaging: '/account/getByPaging',
        getById: '/account/getbyid',
        add: '/account/addaccount',
        update: '/account/updateacount',
        delete: '/account/delelteaccount',
    },
    city: {
        getAll: '/city/getall',
        getByPaging: '/city/getByPaging',
        getById: 'city/getbyid',
        add: '/city/addcity',
        update: '/city/updatecity',
        delete: '/city/deletecity',
    },
    county: {
        getAll: '/county/getall',
        getByPaging: '/county/getByPaging',
        getById: '/county/getbyid',
        add: '/county/addcounty',
        update: '/county/udpatecounty',
        delete: '/county/deletecounty',
    }
};
