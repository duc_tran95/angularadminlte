import { appConstants } from './constants/appConstants';
import { AdminLTEService } from './services/common/admin-lte.service';
import { ErrorInterceptor } from './interceptors/error.interceptor';
import { UserService } from './services/data/user.service';
import { CountyService } from './services/data/county.service';
import { CityService } from './services/data/city.service';
import { LayoutService } from './services/common/layout.service';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AuthenticationInterceptor } from '@app/core/interceptors/authentication.interceptor';
import { AuthenticationGuard } from '@app/core/guards/authentication.guard';
import { AuthenticationService } from '@app/core/services/authentication/authentication.service';
import { AlertService } from '@app/core/services/common/alert.service';
import { LocalStorageService } from '@app/core/services/common/local-storage.service';
import { FormService } from '@app/core/services/common/form.service';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule
  ]
})
export class CoreModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: CoreModule,
            providers: [
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: AuthenticationInterceptor,
                    multi: true
                },
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: ErrorInterceptor,
                    multi: true
                },
                AuthenticationGuard,
                AuthenticationService,
                AdminLTEService,
                AlertService,
                LocalStorageService,
                FormService,
                LayoutService,
                CityService,
                CountyService,
                UserService
            ]
        };
    }
}
