import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTableOption } from '@app/core/models/options/data-table.option';
import { PopupOption } from '@app/core/models/options/popup.option';
import { PopupComponent } from '@app/shared/components/popup/popup.component';
import { DataTableComponent } from '@app/shared/components/data-table/data-table.component';
import * as _ from 'lodash';
import { CityService } from '@app/core/services/data/city.service';
import { NgForm } from '@angular/forms';
import { CityModel } from '@app/core/models/data/city.model';
import { AlertService } from '@app/core/services/common/alert.service';
import { CityPagingRequest } from '@app/core/models/api-data/city.request';
import { FormService } from '@app/core/services/common/form.service';


@Component({
    selector: 'master-data-city',
    templateUrl: './city.component.html',
    styleUrls: ['./city.component.scss']
})
export class CityComponent implements OnInit {

    //#region Inputs

    //#endregion

    //#region Properties

    dataTableCityOptions: DataTableOption;

    popupAddEditCityOptions: PopupOption;

    popupDeleteCityOptions: PopupOption;

    @ViewChild('popupAddEditCity', { static: false }) popupAddEditCity: PopupComponent;

    @ViewChild('popupDeleteCity', { static: false }) popupDeleteCity: PopupComponent;

    @ViewChild('dataTableCity', { static: false }) dataTableCity: DataTableComponent;

    model: CityModel;

    searchParams: {
        name: string
    };

    pagingRequest: CityPagingRequest;

    //#endregion

    //#region Constructors

    constructor(private cityService: CityService, private alertService: AlertService, public formService: FormService) { }

    //#endregion

    //#region OnInit

    ngOnInit() {
        this.pagingRequest = new CityPagingRequest();

        this.searchParams = {
            name: ''
        };

        this.model = new CityModel();

        this.dataTableCityOptions = new DataTableOption({
            api: this.cityService.getByPaging.bind(this.cityService),
            serverSide: true,
            columns: [
                { title: 'Id', data: 'id' },
                { title: 'Name', data: 'name' },
            ],
            actions: ['Add', 'Edit', 'Delete']
        });

        this.popupAddEditCityOptions = new PopupOption({
            title: 'Add City',
            okText: 'Add'
        });

        this.popupDeleteCityOptions = new PopupOption({
            title: 'Delete City',
            okText: 'Yes',
        });
    }

    //#endregion

    //#region Funtions

    showPopupAdd(addCityForm: NgForm) {
        this.resetForm(addCityForm);
        this.popupAddEditCityOptions.okText = 'Add';
        this.popupAddEditCityOptions.title = 'Add City';
        this.popupAddEditCity.show();
    }

    showPopupEdit(event, addCityForm: NgForm) {
        this.resetForm(addCityForm);
        const data = this.dataTableCity.getRowData<CityModel>(event.currentTarget);
        this.cityService.getById(data.id).subscribe(
            resp => {
                this.model = resp;
                this.popupAddEditCityOptions.okText = 'Update';
                this.popupAddEditCityOptions.title = 'Update City';
                this.popupAddEditCity.show();
            }
        );
    }

    showPopupDelete(event) {
        const data = this.dataTableCity.getRowData<CityModel>(event.currentTarget);
        this.model = data;
        this.popupDeleteCity.show();
    }

    refreshDataTable() {
        this.dataTableCity.refreshData();
    }

    resetForm(addCityForm?: NgForm) {
        if (addCityForm) {
            addCityForm.resetForm();
        }
        this.model = new CityModel();
    }

    onCityFormSubmit(addCityForm: NgForm) {
        if (addCityForm.valid) {
            this.formService.trimSpaces(this.model);
            // Update
            if (this.model.id) {
                this.cityService.update(this.model).subscribe(
                    resp => {
                        this.resetForm();
                        this.popupAddEditCity.hide();
                        this.refreshDataTable();
                    }
                );
            } else {
                this.cityService.add(this.model).subscribe(
                    resp => {
                        this.resetForm();
                        this.popupAddEditCity.hide();
                        this.refreshDataTable();
                    }
                );
            }
        }
    }

    onDeleteCitySubmit(event) {
        if (this.model.id) {
            this.cityService.delete(this.model.id).subscribe(
                resp => {
                    this.resetForm();
                    this.popupDeleteCity.hide();
                    this.refreshDataTable();
                }
            );
        }
    }

    onSearchFormSubmit(searchCityForm: NgForm) {
        if (searchCityForm.valid) {
            this.formService.trimSpaces(this.searchParams);
            this.dataTableCity.search(this.searchParams);
        }
    }

    //#endregion

}
