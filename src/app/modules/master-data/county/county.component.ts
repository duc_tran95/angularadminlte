import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTableOption } from '@app/core/models/options/data-table.option';
import { PopupOption } from '@app/core/models/options/popup.option';
import { PopupComponent } from '@app/shared/components/popup/popup.component';
import { DataTableComponent } from '@app/shared/components/data-table/data-table.component';
import * as _ from 'lodash';
import { CountyService } from '@app/core/services/data/county.service';
import { NgForm } from '@angular/forms';
import { CountyModel } from '@app/core/models/data/county.model';
import { AlertService } from '@app/core/services/common/alert.service';
import { CountyPagingRequest } from '@app/core/models/api-data/county.request';
import { FormService } from '@app/core/services/common/form.service';
import { SelectOptionModel } from '@app/core/models/data/select-option.model';
import { CityService } from '@app/core/services/data/city.service';
import { appConstants } from '@app/core/constants/appConstants';


@Component({
    selector: 'master-data-county',
    templateUrl: './county.component.html',
    styleUrls: ['./county.component.scss']
})
export class CountyComponent implements OnInit {

    //#region Inputs

    //#endregion

    //#region Properties

    appConstants = appConstants;

    dataTableCountyOptions: DataTableOption;

    popupAddEditCountyOptions: PopupOption;

    popupDeleteCountyOptions: PopupOption;

    @ViewChild('popupAddEditCounty', { static: false }) popupAddEditCounty: PopupComponent;

    @ViewChild('popupDeleteCounty', { static: false }) popupDeleteCounty: PopupComponent;

    @ViewChild('dataTableCounty', { static: false }) dataTableCounty: DataTableComponent;

    model: CountyModel;

    dropDownList: {
        cityList: SelectOptionModel[]
    };

    searchParams: {
        name: string,
        cityId: number
    };

    pagingRequest: CountyPagingRequest;

    //#endregion

    //#region Constructors

    constructor(private countyService: CountyService, private cityService: CityService,
                private alertService: AlertService, public formService: FormService) { }

    //#endregion

    //#region OnInit

    ngOnInit() {
        this.pagingRequest = new CountyPagingRequest();

        this.searchParams = {
            name: '',
            cityId: null
        };

        this.model = new CountyModel();

        this.dropDownList = {
            cityList: []
        };

        this.loadCities();

        this.dataTableCountyOptions = new DataTableOption({
            api: this.countyService.getByPaging.bind(this.countyService),
            serverSide: true,
            columns: [
                { title: 'Id', data: 'id' },
                { title: 'Name', data: 'name' },
            ],
            actions: ['Add', 'Edit', 'Delete']
        });

        this.popupAddEditCountyOptions = new PopupOption({
            title: 'Add County',
            okText: 'Add',
        });

        this.popupDeleteCountyOptions = new PopupOption({
            title: 'Delete County',
            okText: 'Yes',
        });

    }

    //#endregion

    //#region Funtions

    loadCities() {
        this.cityService.getAll().subscribe(
            resp => {
                this.dropDownList.cityList = resp.map(x => new SelectOptionModel(x.id, x.name));
            }
        );
    }

    showPopupAdd(addCountyForm: NgForm) {
        this.resetForm(addCountyForm);
        this.loadCities();
        this.popupAddEditCountyOptions.okText = 'Add';
        this.popupAddEditCountyOptions.title = 'Add County';
        this.popupAddEditCounty.show();
    }

    showPopupEdit(event, addCountyForm: NgForm) {
        this.resetForm(addCountyForm);
        const data = this.dataTableCounty.getRowData<CountyModel>(event.currentTarget);
        this.countyService.getById(data.id).subscribe(
            resp => {
                this.model = resp;
                this.loadCities();
                this.popupAddEditCountyOptions.okText = 'Update';
                this.popupAddEditCountyOptions.title = 'Update County';
                this.popupAddEditCounty.show();
            }
        );
    }

    showPopupDelete(event) {
        const data = this.dataTableCounty.getRowData<CountyModel>(event.currentTarget);
        this.model = data;
        this.popupDeleteCounty.show();
    }

    refreshDataTable() {
        this.loadCities();
        this.dataTableCounty.refreshData();
    }

    resetForm(addCountyForm?: NgForm) {
        if (addCountyForm) {
            addCountyForm.resetForm();
        }
        this.model = new CountyModel();
    }

    onCountyFormSubmit(addCountyForm: NgForm) {
        if (addCountyForm.valid) {
            this.formService.trimSpaces(this.model);
            // Update
            if (this.model.id) {
                this.countyService.update(this.model).subscribe(
                    resp => {
                        this.resetForm();
                        this.popupAddEditCounty.hide();
                        this.refreshDataTable();
                    }
                );
            } else {
                this.countyService.add(this.model).subscribe(
                    resp => {
                        this.resetForm();
                        this.popupAddEditCounty.hide();
                        this.refreshDataTable();
                    }
                );
            }
        }
    }

    onDeleteCountySubmit(event) {
        if (this.model.id) {
            this.countyService.delete(this.model.id).subscribe(
                resp => {
                    this.resetForm();
                    this.popupDeleteCounty.hide();
                    this.refreshDataTable();
                }
            );
        }
    }

    onSearchFormSubmit(searchCountyForm: NgForm) {
        if (searchCountyForm.valid) {
            this.formService.trimSpaces(this.searchParams);
            this.dataTableCounty.search(this.searchParams);
        }
    }

    //#endregion

}
