import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '@app/core/services/authentication/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { AlertService } from '@app/core/services/common/alert.service';
import { LoginModel } from '@app/core/models/data/login.model';
import { appConstants } from '@app/core/constants/appConstants';
import { FormService } from '@app/core/services/common/form.service';


@Component({
  selector: 'user-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  //#region Inputs, Outputs

  //#endregion

  //#region Properties

  appConstants = appConstants;

  model: LoginModel;

  //#endregion

  //#region Constructors

  constructor(private route: ActivatedRoute, private router: Router,
              private authService: AuthenticationService, private alertService: AlertService, public formService: FormService) { }

  //#endregion

  //#region OnInit

  ngOnInit() {
    this.resetForm();
  }

  //#endregion

  //#region Funtions

  resetForm() {
    this.model = new LoginModel();
  }

  login(logInForm: NgForm) {
    if (logInForm.valid) {
      this.authService.logIn(this.model).subscribe(
        resp => {
          const returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
          this.router.navigate([returnUrl]);
        }
      );
    }
  }

  //#endregion

}
